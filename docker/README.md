# Docker Symfony (PHP7.4-FPM - NGINX - MySQL)


Docker-symfony gives you everything you need to develop Symfony applications. This complete stack run with docker and [docker-compose (1.7 or higher)](https://docs.docker.com/compose/).

Executing the Docker Command Without Sudo [here]( https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user) (recommended).

## Installation

Just copy files from this repository to your project main directory and update .env.dist

## Usage

1. Create a `.env` from the `.env.dist` file. Adapt it according to your symfony application

    ```bash
    merge with symfony .env and update COMPOSE_PROJECT_NAME 
    ```
2. Build/run containers

    ```bash
    $ /scripts/start-dev.sh
    ```
3. Update your system host(`/etc/hosts`) file (add symfony.test)
    ```bash
    # UNIX only: get containers IP address
    $ echo $(docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+')
    ```
4. Prepare Symfony app
    1. Update .env DATABASE_URL value
    2. Composer install

        ```bash
        $ scripts/backend.sh
        $ set Symfony 4 rwx permissions `./var/log`
         ```
             HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
             setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var/log
             setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var/log
         ``` 
        $ composer install
        ```

5. Enjoy :-)

* Symfony app: visit [symfony.test:8080](http://symfony.test:8080)  
* Logs (files location): logs/nginx and logs/symfony

## Xdebug configuration(for PhpStorm)

1. Open server configuration in PHPSTORM: File > Settings > Languages & Frameworks > PHP > Servers
2. In the server configuration page press '+' to add a new server configuration
3. Please enter these setings:
    * 'Name' - you can choose whatever you want
    * 'Host' - Please enter the same server_name as you are set on your nginx configuration(docker/nginx/symfony.conf), by default - 'symfony.test'
    * 'PORT' - 80 and 'Debugger' - Xdebug
    * Check the checkbox on 'Use path mappings'
    * Set two path in 'Absolute path on the server':
        * First path '/var/www/symfony'(this directory you set on docker/php7-fpm/Dockerfile 'WORKDIR') must specify your local project directory;
        * Second path 'var/www/symfony/public' must specify your_local_project_directory/public directory
4. Press 'apply' and 'save'
5. Turn on 'Start listening for PHP Debug Connections' on your PhpStorm.

That's it! You are ready for debugging, enjoy!

## Customize

If you want to add optional containers like Redis, PHPMyAdmin... take a look on [doc/custom.md](doc/custom.md).

## How it works?

Have a look at the `docker-compose.yml` file, here are the `docker-compose` built images:

* `db`: This is the MySQL database container,
* `php`: This is the PHP-FPM container in which the application volume is mounted,
* `nginx`: This is the Nginx webserver container in which application volume is mounted too,

This results in the following running containers:

```bash
$ docker-compose ps
           Name                          Command               State              Ports            
--------------------------------------------------------------------------------------------------
dockersymfony_db_1            /entrypoint.sh mysqld            Up      0.0.0.0:3307->3306/tcp          
dockersymfony_nginx_1         nginx                            Up      443/tcp, 0.0.0.0:8080->80/tcp
dockersymfony_php_1           php-fpm                          Up      0.0.0.0:9000->9000/tcp      
```

## Useful commands

```bash
# View specific container logs
$ docker ps -a
$ docker logs CONTAINER_ID

# bash commands
$ docker-compose -f ./docker/docker-compose.yml exec php bash

# Composer (e.g. composer update)
$ docker-compose -f ./docker/docker-compose.yml exec php composer update

# SF commands (Tips: there is an alias inside php container)
$ docker-compose -f ./docker/docker-compose.yml exec php php /var/www/symfony/bin/console cache:clear # Symfony3
# Same command by using alias
$ docker-compose -f ./docker/docker-compose.yml exec php bash
$ sf cache:clear

# Retrieve an IP Address (here for the nginx container)
$ docker inspect --format '{{ .NetworkSettings.Networks.dockersymfony_default.IPAddress }}' $(docker ps -f name=nginx -q)
$ docker inspect $(docker ps -f name=nginx -q) | grep IPAddress

# MySQL commands
$ docker-compose -f ./docker/docker-compose.yml exec db mysql -uroot -p"root"

# Check CPU consumption
$ docker stats $(docker inspect -f "{{ .Name }}" $(docker ps -q))

# Delete all containers
$ docker rm $(docker ps -aq)

# Delete all images
$ docker rmi $(docker images -q)
```
