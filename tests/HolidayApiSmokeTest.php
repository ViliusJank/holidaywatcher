<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;

class SmokeTest extends WebTestCase
{
    /**
     * Smoke test for the api endpoints.
     *
     * @param string $apiEndpoints
     * @return void
     * @dataProvider getApiEndpoints
     */
    public function testApiEndpoints(string $apiEndpoints)
    {
        $browser = new HttpBrowser(HttpClient::create());

        $browser->request('GET', $apiEndpoints);

        $this->assertEquals(200, $browser->getResponse()->getStatusCode());
    }

    /**
     * Gets the urls for the API endpoints
     *
     * @return void
     */
    public function getApiEndpoints()
    {
        return [
            [$_ENV['HOLIDAY_API_URL']],
            [$_ENV['HOLIDAY_API_URL'] . '?action=getHolidaysForMonth&month=1&year=2022&country=deu&region=bw&holidayType=public_holiday'],
            [$_ENV['HOLIDAY_API_URL'] . '?action=isPublicHoliday&date=05-07-2022&country=svk'],
            [$_ENV['HOLIDAY_API_URL'] . '?action=isWorkDay&date=30-07-2022&country=hun'],
            [$_ENV['HOLIDAY_API_URL'] . '?action=getSupportedCountries']
        ];
    }
}
