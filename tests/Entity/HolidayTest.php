<?php

namespace App\Tests;

use App\Entity\Country;
use App\Entity\Holiday;
use DateTime;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotSame;
use function PHPUnit\Framework\assertSame;

class HolidayTest extends TestCase
{
    private $holiday;

    public function setUp(): void
    {
        $this->holiday = new Holiday();
    }

    /**
     * @dataProvider getHolidayCreationTests
     */
    public function testHolidayCreation(
        string $holidayName,
        DateTime $date,
        string $holidayType,
        Country $country
    ) {
        $this->holiday->setName($holidayName);
        $this->holiday->setDate($date);
        $this->holiday->setHolidayType($holidayType);
        $this->holiday->setCountry($country);

        $otherCountry = new Country();

        assertSame($holidayName, $this->holiday->getName());
        assertSame($date, $this->holiday->getDate());
        assertSame($holidayType, $this->holiday->getHolidayType());

        assertSame($country, $this->holiday->getCountry());
        assertNotSame($otherCountry, $this->holiday->getCountry());
    }

    /**
     * @dataProvider getHolidayPrettyTypeTests
     */
    public function testHolidayPrettyName(
        string $holidayType,
        string $holidayPrettyName
    ) {
        $this->holiday->setHolidayType($holidayType);

        assertSame($holidayPrettyName, $this->holiday->getHolidayTypePrettyName());
    }

    public function getHolidayCreationTests()
    {
        return [
            ['New year', new DateTime('2000-02-02'), Holiday::EXTRA_WORKING_DAY_TYPE, new Country()],
            ['Easter', new DateTime('2012-04-04'), Holiday::SCHOOL_HOLIDAY_TYPE, new Country()],
            ['Christmas', new DateTime('2011-02-12'), Holiday::HOLIDAY_TYPE, new Country()],
        ];
    }

    public function getHolidayPrettyTypeTests()
    {
        return [
            [Holiday::HOLIDAY_TYPE, Holiday::getHolidayTypeList()[Holiday::HOLIDAY_TYPE]],
            [Holiday::EXTRA_WORKING_DAY_TYPE, Holiday::getHolidayTypeList()[Holiday::EXTRA_WORKING_DAY_TYPE]],
            [Holiday::ALL_TYPE, Holiday::getHolidayTypeList()[Holiday::ALL_TYPE]],
        ];
    }
}
