<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200629124834 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE holidays ADD country_id INT NOT NULL, DROP country');
        $this->addSql('ALTER TABLE holidays ADD CONSTRAINT FK_3A66A10CF92F3E70 FOREIGN KEY (country_id) REFERENCES countries (id)');
        $this->addSql('CREATE INDEX IDX_3A66A10CF92F3E70 ON holidays (country_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE holidays DROP FOREIGN KEY FK_3A66A10CF92F3E70');
        $this->addSql('DROP INDEX IDX_3A66A10CF92F3E70 ON holidays');
        $this->addSql('ALTER TABLE holidays ADD country VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP country_id');
    }
}
