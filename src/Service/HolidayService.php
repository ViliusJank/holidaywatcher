<?php

namespace App\Service;

use App\Denormalizer\HolidayDenormalizer;
use App\Entity\Holiday;
use App\Repository\CountryRepository;
use App\Repository\HolidayRepository;
use App\Utils\HolidayUtils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;

class HolidayService
{
    public const WORKDAY = 'Workday';
    public const HOLIDAY = 'Holiday';
    public const FREEDAY = 'Freeday';

    private $em;
    private $holidayRepository;
    private $countryRepository;
    private $client;
    private $apiUrl;
    private $logger;


    public function __construct(
        EntityManagerInterface $em,
        HolidayRepository $holidayRepository,
        CountryRepository $countryRepository,
        LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->holidayRepository = $holidayRepository;
        $this->countryRepository = $countryRepository;
        $this->client = HttpClient::create();
        $this->apiUrl = $_ENV['HOLIDAY_API_URL'];
        $this->logger = $logger;
    }

    /**
     * Gets holidays from API if no data is found in the DB.
     *
     * @param string $countryCode
     * @param integer $year
     * @return array $holidays
     */
    public function getHolidays(string $countryCode, int $year): array
    {
        $holidays = $this->holidayRepository->findByCountryAndYear($countryCode, $year);
        if (empty($holidays)) {
            $denormalizer = new HolidayDenormalizer();
            $country = $this->countryRepository->findOneBy(['code' => $countryCode]);

            $response = $this->client->request('GET', $this->apiUrl, [
                'query' => [
                    'action' => 'getHolidaysForYear',
                    'year' => $year,
                    'country' => $countryCode,
                ],
            ]);

            if ($response->getStatusCode() === 200) {
                $payloadArray = $response->toArray();
                try {
                    foreach ($payloadArray as $holiday) {
                        $holiday = $denormalizer->denormalize($holiday, Holiday::class, 'json');
                        $holiday->setCountry($country);
                        $holidays[] = $holiday;

                        $this->em->persist($holiday);
                    }
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage());
                }

                $this->em->flush();
            } else {
                $holidays = [];
            }
        }

        return $holidays;
    }

    /**
     * Get the specific day status (workday, holiday, freeday)
     *
     * @param DateTime $date
     * @param array $holidays
     * @param string $countryCode
     * @return string $dayStatus
     */
    public function getDayStatus(DateTime $date, array $holidays, string $countryCode = ''): string
    {
        if (!empty($countryCode)) {
            $holidays = $this->getHolidays($countryCode, $date->format('Y'));
        }

        foreach ($holidays as $holiday) {
            if ($holiday->getDate() == $date) {
                $dayStatus = self::HOLIDAY;
            }
        }

        if (!isset($dayStatus)) {
            $dayStatus = HolidayUtils::isFreeday($date) ? self::FREEDAY : self::WORKDAY;
        }

        return $dayStatus;
    }

    /**
     * Gets the maximum number of free(free day + holiday) days in a row, which will still be in the selected country year.
     *
     * @param integer $year
     * @param string $countryCode
     * @return void
     */
    public function getLongestSeriesOfFreedaysCount(int $year, string $countryCode)
    {
        $maxSerialFreedays = 2; // Minimum longest series is the weekends 2 days.
        $currentDate = new DateTime();
        $fromDate = DateTime::createFromFormat('d-m-Y', '01-01-' . $year);
        $toDate = (clone $currentDate)->modify('last day of December this year');
        $holidays = [];

        if ($year != $currentDate->format('Y')) {
            $holidays = $this->getHolidays($countryCode, $year);
        } else {
            $holidays = $this->getHolidaysInDateRange($countryCode, $currentDate, $toDate);
        }

        foreach ($holidays as $holiday) {
            $serialFreedayCount = $this->getSurroundingFreedayCount($holiday, $holidays, $fromDate, $toDate);
            $maxSerialFreedays = ($serialFreedayCount > $maxSerialFreedays) ? $serialFreedayCount : $maxSerialFreedays;
        }

        return $maxSerialFreedays;
    }

    /**
     * Gets the freedays that surround a specific day.
     *
     * @param Holiday $holiday
     * @param array $holidays
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return void
     */
    private function getSurroundingFreedayCount(
        Holiday $holiday,
        array $holidays,
        DateTime $fromDate,
        DateTime $toDate
    ) {
        $freedayCount = 1;
        $nextDayDate = clone $holiday->getDate();
        $previousDayDate = clone $holiday->getDate();

        // TODO: Recursive function would be better.
        do {
            $nextDayDate = $nextDayDate->modify('+1 day');
            $nextDayStatus = $this->getDayStatus($nextDayDate, $holidays);

            if ($toDate < $nextDayDate) {
                break;
            }

            if ($nextDayStatus == self::FREEDAY || $nextDayStatus == self::HOLIDAY) {
                $freedayCount++;
            }
        } while ($nextDayStatus == self::FREEDAY || $nextDayStatus == self::HOLIDAY);

        do {
            $previousDayDate = $previousDayDate->modify('-1 day');
            $previousDayStatus = $this->getDayStatus($previousDayDate, $holidays);

            if ($fromDate > $previousDayDate) {
                break;
            }

            if ($previousDayStatus == self::FREEDAY || $previousDayStatus == self::HOLIDAY) {
                $freedayCount++;
            }
        } while ($previousDayStatus == self::FREEDAY || $previousDayStatus == self::HOLIDAY);

        return $freedayCount;
    }

    /**
     * Gets holidays of a country in a specific date range.
     *
     * @param string $countryCode
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return void
     */
    public function getHolidaysInDateRange(
        string $countryCode,
        DateTime $fromDate,
        DateTime $toDate
    ) {
        $holidays = [];
        $denormalizer = new HolidayDenormalizer();

        $response = $this->client->request('GET', $this->apiUrl, [
            'query' => [
                'action' => 'getHolidaysForDateRange',
                'country' => $countryCode,
                'fromDate' => $fromDate->format('d-m-Y'),
                'toDate' => $toDate->format('d-m-Y'),
            ],
        ]);

        if ($response->getStatusCode() === 200) {
            $payloadArray = $response->toArray();
            try {
                foreach ($payloadArray as $holiday) {
                    $holiday = $denormalizer->denormalize($holiday, Holiday::class, 'json');
                    $holidays[] = $holiday;
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        return $holidays;
    }
}
