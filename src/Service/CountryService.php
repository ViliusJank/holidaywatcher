<?php

namespace App\Service;

use App\Assembler\EntityAssembler;
use App\Model\Country\CountryModel;
use App\Repository\CountryRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CountryService
{
    private $countryRepository;
    private $client;
    private $apiUrl;
    private $em;
    private $logger;
    private $serializer;
    private $entityAssembler;

    public function __construct(
        CountryRepository $countryRepository,
        EntityManagerInterface $em,
        LoggerInterface $logger
    ) {
        $this->countryRepository = $countryRepository;
        $this->client = HttpClient::create();
        $this->apiUrl = $_ENV['HOLIDAY_API_URL'];
        $this->em = $em;
        $this->logger = $logger;

        $normalizers = [new ObjectNormalizer(null, null, null, new ReflectionExtractor())];
        $this->serializer = new Serializer($normalizers);
        $this->entityAssembler = new EntityAssembler();
    }

    /**
     * Saves countries that the holiday API has data about to DB.
     *
     * @return boolean $success
     */
    public function registerAvailableCountries(): bool
    {
        $success = true;
        $response = $this->client->request('GET', $this->apiUrl, [
            'query' => [
                'action' => 'getSupportedCountries',
            ],
        ]);

        if ($response->getStatusCode() === 200) {
            try {
                $payloadArray = $response->toArray();

                foreach ($payloadArray as $country) {
                    $countryModel = $this->serializer->denormalize($country, CountryModel::class, 'json');
                    $country = $this->entityAssembler->assembleCountry($countryModel);

                    $this->em->persist($country);
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $success = false;
            }

            $this->em->flush();
        } else {
            $success = false;
        }

        return $success;
    }

    /**
     * Gets all countries inside DB.
     *
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countryRepository->findAll();
    }

    /**
     * Gets the date from when country holidays are documented.
     *
     * @param string $countryCode
     * @return DateTime|null
     */
    public function getCountryFromDate(string $countryCode): ?DateTime
    {
        $country = $this->countryRepository->findOneBy(['code' => $countryCode]);

        return $country ? $country->getFromDate() : null;
    }
}
