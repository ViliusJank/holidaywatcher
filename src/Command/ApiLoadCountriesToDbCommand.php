<?php

namespace App\Command;

use App\Service\CountryService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ApiLoadCountriesToDbCommand extends Command
{
    protected static $defaultName = 'api:load-countries-to-db';
    private $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Loads countries supported by the api to the database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($this->countryService->registerAvailableCountries()) {
            $io->success('Countries loaded to database successfully.');
        } else {
            $io->error('Failed to load countries.');
        }

        return 0;
    }
}
