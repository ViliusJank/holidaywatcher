<?php

namespace App\Entity;

use App\Repository\HolidayRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=HolidayRepository::class)
 * @ORM\Table(name="holidays",
 *  uniqueConstraints={@ORM\UniqueConstraint(columns={
 *      "name", "date", "country_id"
 *  })}
 * )
 * @UniqueEntity(
 *     fields={"name", "date", "country"},
 *     message="Holiday already exists."
 * )
 */
class Holiday
{
    public const HOLIDAY_TYPE = 'public_holiday';
    public const ALL_TYPE = 'all';
    public const OBSERVANCE_TYPE = 'observance';
    public const SCHOOL_HOLIDAY_TYPE = 'school_holiday';
    public const OTHER_DAY_TYPE = 'other_day';
    public const EXTRA_WORKING_DAY_TYPE = 'extra_working_day';

    /**
     * Returns holiday type list array.
     *
     * @return array
     */
    public static function getHolidayTypeList()
    {
        return [
            self::HOLIDAY_TYPE => 'Holiday',
            self::ALL_TYPE => 'All',
            self::OBSERVANCE_TYPE => 'Observance',
            self::SCHOOL_HOLIDAY_TYPE => 'School holiday',
            self::OTHER_DAY_TYPE => 'Other day',
            self::EXTRA_WORKING_DAY_TYPE => 'Extra working day'
        ];
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $holiday_type;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="holidays")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHolidayType(): ?string
    {
        return $this->holiday_type;
    }

    public function getHolidayTypePrettyName(): ?string
    {
        $holiday_type = $this->getHolidayType();
        if ($holiday_type) {
            return self::getHolidayTypeList()[$holiday_type];
        } else {
            return 'No name';
        }
    }

    public function setHolidayType(string $holiday_type): self
    {
        $this->holiday_type = $holiday_type;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }
}
