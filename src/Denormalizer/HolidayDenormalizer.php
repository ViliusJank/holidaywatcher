<?php

namespace App\Denormalizer;

use App\Entity\Holiday;
use DateTime;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class HolidayDenormalizer implements DenormalizerInterface
{
    /**
     * Denormalizes data back into an object of the given class.
     *
     * @param mixed  $data    Data to restore
     * @param string $type    The expected class to instantiate
     * @param string $format  Format the given data was extracted from
     * @param array  $context Options available to the denormalizer
     *
     * @return object|array
     *
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $holiday = new Holiday();
        $dateString = implode("/", [$data['date']['day'], $data['date']['month'], $data['date']['year']]);
        $holiday->setDate(DateTime::createFromFormat("d/m/Y", $dateString));
        $holiday->setHolidayType($data['holidayType']);

        foreach ($data['name'] as $name) {
            if ($name['lang'] == 'en') {
                $holidayName = $name['text'];
                break;
            }
        }

        // In case english version not available pick the first name.
        if (!$holidayName && !empty($data['name'])) {
            $holidayName = $data['name'][0]['text'];
        }

        $holiday->setName($holidayName);

        return $holiday;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed  $data   Data to denormalize from
     * @param string $type   The class to which the data should be denormalized
     * @param string $format The format being deserialized from
     *
     * @return bool
     */
    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $type instanceof Holiday;
    }
}
