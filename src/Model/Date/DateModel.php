<?php

namespace App\Model\Date;

class DateModel
{
    /**
     * @var int
     */
    private $day;

    /**
     * @var int
     */
    private $month;

    /**
     * @var int
     */
    private $year;

    public function setDay($day)
    {
        $this->day = $day;
    }

    public function getDay() {
        return $this->day;
    }

    public function getMonth() {
        return $this->month;
    }

    public function setMonth($month)
    {
        $this->month = $month;
    }

    public function getYear() {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }
}
