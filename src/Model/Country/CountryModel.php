<?php

namespace App\Model\Country;

use App\Model\Date\DateModel;

class CountryModel
{
    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var DateModel
     */
    private $fromDate;

    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    public function getCountryCode() {
        return $this->countryCode;
    }

    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function setFromDate(DateModel $fromDate)
    {
        $this->fromDate = $fromDate;
    }

    public function getFromDate(): DateModel
    {
        return $this->fromDate;
    }
}
