<?php

namespace App\Utils;

use DateTime;

class HolidayUtils
{
    /**
     * Checks if day is freeday.
     *
     * @param DateTime $date
     * @return boolean
     */
    public static function isFreeday(DateTime $date): bool
    {
        return ($date->format('N') >= 6);
    }

    /**
     * Counts the public holidays inside holiday entity array.
     *
     * @param array $holidays
     * @return integer
     */
    public static function countPublicHolidays(array $holidays): int
    {
        return count(array_filter($holidays, function ($holiday) {
            return $holiday->getHolidayType() == 'public_holiday';
        }));
    }

    /**
     * Groups holiday array by month.
     *
     * @param array $holidays
     * @return array
     */
    public static function groupByMonth(array $holidays): array
    {

        $groupedHolidays = array();

        foreach ($holidays as $holiday) {
            $month = $holiday->getDate()->format('F');
            $groupedHolidays[$month][] = $holiday;
        }

        return $groupedHolidays;
    }
}
