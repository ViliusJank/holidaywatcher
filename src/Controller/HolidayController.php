<?php

namespace App\Controller;

use App\Repository\CountryRepository;
use App\Service\CountryService;
use App\Service\HolidayService;
use App\Utils\HolidayUtils;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HolidayController extends AbstractController
{
    private $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }


    /**
     * @Route("/holiday-search", name="holiday-search", methods={"GET","POST"})
     */
    public function search(Request $request, HolidayService $holidayService)
    {
        $countries = $this->countryService->getCountries();

        if ($request->isMethod('POST')) {
            $selectedCountry = $request->get('country');
            $selectedYear = $request->get('year');

            $holidays = $holidayService->getHolidays($selectedCountry, $selectedYear);
            $maxSeriesFreedays = $holidayService->getLongestSeriesOfFreedaysCount($selectedYear, $selectedCountry);
            $currentDayStatus = $holidayService->getDayStatus((new DateTime()), [], $selectedCountry);

            return $this->render('pages/holiday_search.html.twig', [
                'holidayMonthGroups' => HolidayUtils::groupByMonth($holidays),
                'countries' => $countries,
                'publicHolidayCount' => HolidayUtils::countPublicHolidays($holidays),
                'selectedCountry' => $selectedCountry,
                'currentDayStatus' => $currentDayStatus,
                'maxSeriesFreedays' => $maxSeriesFreedays
            ]);
        }

        return $this->render('pages/holiday_search.html.twig', [
            'countries' => $countries,
        ]);
    }

    /**
     * @Route("ajax/country-from/{countryCode}", options={"expose" = true}, name="ajax_get_country_available_from")
     */
    public function getCountryAvailableFrom(string $countryCode)
    {
        $dateFrom = $this->countryService->getCountryFromDate($countryCode);

        $statusCode = $dateFrom ? 200 : 404;
        $payload = $dateFrom ? $dateFrom : "Country not found.";

        return new JsonResponse($payload, $statusCode);
    }
}
