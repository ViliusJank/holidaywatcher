<?php

namespace App\Assembler;

use App\Entity\Country;
use App\Model\Country\CountryModel;
use DateTime;

/**
 * Class for converting models to etities.
 */
class EntityAssembler
{
    /**
     * Converts country model to country entity.
     *
     * @param CountryModel $countryModel
     * @return Country $country
     */
    public function assembleCountry(CountryModel $countryModel): Country
    {
        $dateModel = $countryModel->getFromDate();
        $dateString = sprintf('%d-%d-%d', $dateModel->getDay(), $dateModel->getMonth(), $dateModel->getYear());
        $date = DateTime::createFromFormat('d-m-Y', $dateString);

        $country = new Country();
        $country->setCode($countryModel->getCountryCode());
        $country->setFullName($countryModel->getFullName());
        $country->setFromDate($date);

        return $country;
    }
}
