<?php

namespace App\Repository;

use App\Entity\Holiday;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Holiday|null find($id, $lockMode = null, $lockVersion = null)
 * @method Holiday|null findOneBy(array $criteria, array $orderBy = null)
 * @method Holiday[]    findAll()
 * @method Holiday[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HolidayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Holiday::class);
    }

    /**
     * @param string $countryCode The country code.
     * @param int $year The year.
     * @return Holiday[] Returns an array of Holiday objects
     */
    public function findByCountryAndYear($countryCode, $year)
    {
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('c.code', $countryCode));

        return $this->createQueryBuilder('h')
            ->innerJoin('h.country', 'c')
            ->addCriteria($criteria)
            ->andWhere('YEAR(h.date) = :year')
            ->setParameter('year', $year)
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @param string $countryCode The country code.
    //  * @param int $year The year.
    //  * @return Holiday[] Returns an array of Holiday objects
    //  */
    // public function findByCountryAndYearGroupByMonth($countryCode, $year)
    // {
    //     $criteria = Criteria::create()
    //         ->andWhere(Criteria::expr()->eq('c.code', $countryCode))
    //         ->orderBy(['h.date' => 'ASC']);


    //     return $this->createQueryBuilder('h')
    //         ->select('h', 'MONTH(h.date) as month')
    //         ->innerJoin('h.country', 'c')
    //         ->addCriteria($criteria)
    //         ->andWhere('YEAR(h.date) = :year')
    //         ->setParameter('year', $year)
    //         ->GroupBy('month')
    //         ->getQuery()
    //         ->getResult();
    // }
}
