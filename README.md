Holiday watcher
========================

An app for getting global holiday information.

Requirements
========================

- PHP 7.3
- Apache/Nginx
- MySQL

Development environment
========================

1. cd holiday_watcher
2. set rwx permissions `./var/cache ./var/logs ./web/uploads` [Setting up or Fixing File Permissions](http://symfony.com/doc/2.8/setup/file_permissions.html)

Deployment
========================

* Project uses [Deployer](https://deployer.org/) as auto-deployment tool.
- staging: `php ./deployer.phar deploy staging` 
- prod: `php ./deployer.phar deploy prod`


Tests
========================

1. run `vendor/bin/phpunit`