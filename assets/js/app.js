/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';
import $ from 'jquery';
import 'bootstrap';
import 'select2';


// Engage select2 dropdowns.
$('select').select2();

// Populate available country dates
$(document).ready(function () {
    var $countrySelect = $('#countrySelect');
    var $yearSelect = $('#yearSelect');
    $countrySelect.on('change', function (e) {
        e.preventDefault();
        $.ajax({
            method: 'GET',
            url: Routing.generate('ajax_get_country_available_from', { countryCode: $countrySelect.select2('val') }),
        }).done(function (data) {
            $yearSelect.empty();
            var fromYear = new Date(data.date).getFullYear();
            var currentYear = new Date().getFullYear();

            for (var index = fromYear; index <= currentYear; index++) {
                $yearSelect.append($('<option>', {
                    value: index,
                    text: index
                }));
            }
        });
    });

    $countrySelect.trigger('change');
});